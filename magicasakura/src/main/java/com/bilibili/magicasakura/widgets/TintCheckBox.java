/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.bilibili.magicasakura.utils.TintManager;

/**
 * @author xyczero617@gmail.com
 * @time 16/1/27
 */
public class TintCheckBox extends Checkbox implements Tintable, AppCompatBackgroundHelper.BackgroundExtensible,
    AppCompatCompoundButtonHelper.CompoundButtonExtensible, AppCompatTextHelper.TextExtensible {
    private AppCompatBackgroundHelper mBackgroundHelper;
    private AppCompatCompoundButtonHelper mCompoundButtonHelper;
    private AppCompatTextHelper mTextHelper;
    private Context mContext;
    private BindStateChangedListener mBindStateChangedListener = new BindStateChangedListener() {
        @Override
        public void onComponentBoundToWindow(Component component) {
        }

        @Override
        public void onComponentUnboundFromWindow(Component component) {

        }
    };

    public TintCheckBox(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public TintCheckBox(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, null);
        this.mContext = context;
        setBindStateChangedListener(mBindStateChangedListener);
        TintManager tintManager = TintManager.get(getContext());

        mBackgroundHelper = new AppCompatBackgroundHelper(this, tintManager);
        mBackgroundHelper.loadFromAttribute(attrs, defStyleAttr);

        mCompoundButtonHelper = new AppCompatCompoundButtonHelper(this, tintManager);
        mCompoundButtonHelper.loadFromAttribute(attrs, defStyleAttr);

        mTextHelper = new AppCompatTextHelper(this, tintManager);
        mTextHelper.loadFromAttribute(attrs, defStyleAttr);
    }

    @Override
    public void setTextColor(Color color) {
        super.setTextColor(color);
        if (mTextHelper != null) {
            mTextHelper.setTextColor();
        }
    }

    public void setTextAppearance(int resId) {
        if (mTextHelper != null) {
            mTextHelper.setTextAppearanceForTextColor(resId);
        }
    }

    public void setBackgroundDrawable(Element background) {
        super.setBackground(background);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundRes(background);
        }
    }

    public void setBackgroundResource(int resId) {
        ShapeElement shapeElement = new ShapeElement(mContext, resId);
        super.setBackground(shapeElement);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundRes(shapeElement);
        }
    }

    public void setBackgroundColor(int color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        super.setBackground(shapeElement);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundColor(color);
        }
    }

    @Override
    public void setButtonElement(Element element) {
        super.setButtonElement(element);
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.setButtonDrawable();
        }
    }

    @Override
    public void tint() {
        if (mTextHelper != null) {
            mTextHelper.tint();
        }
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.tint();
        }
        if (mBackgroundHelper != null) {
            mBackgroundHelper.tint();
        }
    }

    @Override
    public void setBackgroundTintList(Color color) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color,BlendMode.SRC);
        }
    }

    @Override
    public void setBackgroundTintList(Color color, BlendMode mode) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color,mode);
        }
    }

    @Override
    public void setCompoundButtonTintList(Color color) {
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.setButtonDrawableTintList(color,BlendMode.SRC);
        }
    }

    @Override
    public void setCompoundButtonTintList(Color color, BlendMode mode) {
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.setButtonDrawableTintList(color,mode);
        }
    }

    @Override
    public void setTextColorByColor(Color color) {
        if (mTextHelper != null) {
            mTextHelper.setTextColorByColor(color);
        }
    }
}
