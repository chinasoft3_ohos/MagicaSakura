/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;

import com.bilibili.magicasakura.utils.AttrValue;
import com.bilibili.magicasakura.utils.DrawableUtils;
import com.bilibili.magicasakura.utils.ElementTintUtil;
import com.bilibili.magicasakura.utils.ThemeUtils;
import com.bilibili.magicasakura.utils.TintInfo;
import com.bilibili.magicasakura.utils.TintManager;

/**
 * @author xyczero617@gmail.com
 * @time 16/4/7
 */
class AppCompatForegroundHelper extends AppCompatBaseHelper<Component> {

    private TintInfo mForegroundTintInfo;

    private Element mForegroundRes;
    private Color mForegroundTintRes;

    AppCompatForegroundHelper(Component view, TintManager tintManager) {
        super(view, tintManager);
    }

    @SuppressWarnings("ResourceType")
    @Override
    void loadFromAttribute(AttrSet attrs, int defStyleAttr) {

        if (attrs.getAttr("foregroundTint").isPresent()) {
            mForegroundTintRes = AttrValue.get(attrs, "foregroundTint", ThemeUtils.themeColor);
            if (attrs.getAttr("foregroundTintMode").isPresent()) {
                setSupportForegroundTintMode(DrawableUtils.parseTintMode(AttrValue.get(attrs, "foregroundTintMode", 0), null));
            }
            setSupportForegroundTint();
        } else {
            Element drawable = null;
            mForegroundRes = AttrValue.get(attrs, "foreground_element", drawable);
            if (mForegroundRes != null) {
                setForegroundDrawable(mForegroundRes);
            }
        }
    }

    /**
     * External use
     *
     * @param foreground foreground
     */
    public void setForegroundDrawableExternal(Element foreground) {
        if (skipNextApply()) return;

        resetTintResource(null);
        setSkipNextApply(false);
    }

    public void setForegroundResId(Element element) {
        if (mForegroundRes != element) {
            resetTintResource(element);

            if (element != null) {
                setForegroundDrawable(element);
            }
        }
    }

    public void setForegroundTintList(Color color, BlendMode mode) {
        if (mForegroundTintRes.getValue() != color.getValue()) {
            mForegroundTintRes = color;
            if (mForegroundTintInfo != null) {
                mForegroundTintInfo.mHasTintColor = false;
                mForegroundTintInfo.mTintColor = 0;
            }
            setSupportForegroundTintMode(mode);
            setSupportForegroundTint();
        }
    }

    /**
     * Internal use
     *
     * @param drawable drawable
     */
    private void setForegroundDrawable(Element drawable) {
        if (skipNextApply()) return;

        setForeground(drawable);
    }

    private boolean setSupportForegroundTint() {
        if (mForegroundTintInfo == null) {
            mForegroundTintInfo = new TintInfo();
        }
        mForegroundTintInfo.mHasTintColor = true;
        mForegroundTintInfo.mTintColor = mTintManager.getReplaceColorValue(mForegroundTintRes);
        return applySupportForegroundTint();
    }

    private void setSupportForegroundTintMode(BlendMode mode) {
        if (mForegroundTintRes != null && mode != null) {
            if (mForegroundTintInfo == null) {
                mForegroundTintInfo = new TintInfo();
            }
            mForegroundTintInfo.mHasTintMode = true;
            mForegroundTintInfo.mTintMode = mode;
        }
    }

    private boolean applySupportForegroundTint() {
        Element foregroundDrawable = getForeground();
        if (foregroundDrawable != null && mForegroundTintInfo != null && mForegroundTintInfo.mHasTintColor) {
            if (mForegroundTintInfo.mHasTintColor) {
                ElementTintUtil.setColorTint(foregroundDrawable, mForegroundTintInfo.mTintColor);
            }
            if (mForegroundTintInfo.mHasTintMode) {
                foregroundDrawable.setStateColorMode(mForegroundTintInfo.mTintMode);
            }
            setForegroundDrawable(foregroundDrawable);
            return true;
        }
        return false;
    }

    private Element getForeground() {
        mView.getForegroundElement();
        return null;
    }


    private void setForeground(Element foreground) {
        mView.setForeground(foreground);
    }

    private void resetTintResource(Element element) {
        mForegroundRes = element;
        mForegroundTintRes = null;
        if (mForegroundTintInfo != null) {
            mForegroundTintInfo.mHasTintList = false;
            mForegroundTintInfo.mTintList = null;
            mForegroundTintInfo.mHasTintMode = false;
            mForegroundTintInfo.mTintMode = null;
        }
    }

    @Override
    public void tint() {
        setSupportForegroundTint();
    }

    public interface ForegroundExtensible {
        void setForegroundTintList(Color color);

        void setForegroundTintList(Color color, BlendMode mode);
    }
}
