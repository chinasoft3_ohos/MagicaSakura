/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.RadioButton;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.bilibili.magicasakura.utils.ThemeUtils;
import com.bilibili.magicasakura.utils.TintManager;

/**
 * @author xyczero617@gmail.com
 * @time 16/1/19
 */
public class TintRadioButton extends RadioButton implements Tintable, AppCompatBackgroundHelper.BackgroundExtensible,
    AppCompatCompoundButtonHelper.CompoundButtonExtensible, AppCompatTextHelper.TextExtensible, Component.ComponentStateChangedListener {
    private AppCompatTextHelper mTextHelper;
    private AppCompatBackgroundHelper mBackgroundHelper;
    private AppCompatCompoundButtonHelper mCompoundButtonHelper;

    public TintRadioButton(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    public TintRadioButton(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setComponentStateChangedListener(this);
        TintManager tintManager = TintManager.get(context);

        mTextHelper = new AppCompatTextHelper(this, tintManager);
        mTextHelper.loadFromAttribute(attrs, 0);

        mBackgroundHelper = new AppCompatBackgroundHelper(this, tintManager);
        mBackgroundHelper.loadFromAttribute(attrs, 0);

        mCompoundButtonHelper = new AppCompatCompoundButtonHelper(this, tintManager);
        mCompoundButtonHelper.loadFromAttribute(attrs, 0);
    }

    @Override
    public void onComponentStateChanged(Component component, int i) {
    }

    @Override
    public void setTextColor(Color color) {
        super.setTextColor(color);
        if (mTextHelper != null) {
            mTextHelper.setTextColor();
        }
    }

    @Override
    public void setBackground(Element element) {
        super.setBackground(element);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundDrawableExternal(element);
        }
    }

    @Override
    public void setButtonElement(Element element) {
        super.setButtonElement(element);
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.setButtonDrawable();
        }
    }

    @Override
    public int getPaddingLeft() {
        final int value = super.getPaddingLeft();
        return mCompoundButtonHelper != null
            ? mCompoundButtonHelper.getCompoundPaddingLeft(value)
            : value;
    }

    @Override
    public void tint() {
        if (mTextHelper != null) {
            mTextHelper.tint();
        }
        if (mBackgroundHelper != null) {
            mBackgroundHelper.tint();
        }
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.tint();
        }
    }

    @Override
    public void setBackgroundTintList(Color color) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, BlendMode.SRC);
        }
    }

    @Override
    public void setBackgroundTintList(Color color, BlendMode mode) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, mode);
        }
    }

    @Override
    public void setCompoundButtonTintList(Color color) {
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.setButtonDrawableTintList(color, BlendMode.SRC_IN);
        }
    }

    @Override
    public void setCompoundButtonTintList(Color color, BlendMode mode) {
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.setButtonDrawableTintList(color, mode);
        }
    }

    @Override
    public void setTextColorByColor(Color color) {
        if (mTextHelper != null) {
            mTextHelper.setTextColorByColor(color);
        }
    }
}
