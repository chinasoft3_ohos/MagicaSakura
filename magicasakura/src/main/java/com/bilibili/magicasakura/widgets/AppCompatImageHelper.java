/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;

import com.bilibili.magicasakura.utils.AttrValue;
import com.bilibili.magicasakura.utils.DrawableUtils;
import com.bilibili.magicasakura.utils.ElementTintUtil;
import com.bilibili.magicasakura.utils.ThemeUtils;
import com.bilibili.magicasakura.utils.TintInfo;
import com.bilibili.magicasakura.utils.TintManager;

/**
 * @author xyczero617@gmail.com
 * @time 15/11/15
 */
class AppCompatImageHelper extends AppCompatBaseHelper<Image> {

    private TintInfo mImageTintInfo;
    private Element mElementRes;
    private Color mImageTintRes;

    AppCompatImageHelper(Image view, TintManager tintManager) {
        super(view, tintManager);
    }

    @SuppressWarnings("ResourceType")
    @Override
    void loadFromAttribute(AttrSet attrs, int defStyleAttr) {
//        // first resolve srcCompat due to not extending by AppCompatImageView
        Element drawable = null;
        if (mView.getImageElement() == null) {
            Element element = AttrValue.get(attrs, "srcCompat", drawable);
            setImageDrawable(element);
        }

        if (attrs.getAttr("imageTint").isPresent()) {
            mImageTintRes = AttrValue.get(attrs, "imageTint", ThemeUtils.themeColor);
            if (attrs.getAttr("imageTintMode").isPresent()) {
                setSupportImageTintMode(DrawableUtils.parseTintMode(AttrValue.get(attrs, "imageTintMode", 0), null));
            }
            setSupportImageTint();
        } else {
            mElementRes = AttrValue.get(attrs, "image_src", drawable);
            setImageDrawable(mElementRes);
        }
    }

    /**
     * External use
     */
    public void setImageDrawable() {
        if (skipNextApply()) return;

        resetTintResource(null);
        setSkipNextApply(false);
    }

    public void setImageRes(Element element) {
        if (mElementRes != element) {
            resetTintResource(element);
            if (element != null) {
                setImageDrawable(element);
            }
        }
    }

    public void setImageTintList(Color tintColor, BlendMode mode) {
        if (mImageTintRes == null || mImageTintRes.getValue() != tintColor.getValue()) {
            mImageTintRes = tintColor;
            if (mImageTintInfo != null) {
                mImageTintInfo.mHasTintColor = false;
                mImageTintInfo.mTintColor = 0;
            }
            setSupportImageTintMode(mode);
            setSupportImageTint();
        }
    }

    /**
     * Internal use
     *
     * @param drawable drawable
     */
    private void setImageDrawable(Element drawable) {
        if (skipNextApply()) return;
        mView.setImageElement(drawable);
    }

    private boolean setSupportImageTint() {
        if (mImageTintInfo == null) {
            mImageTintInfo = new TintInfo();
        }
        mImageTintInfo.mHasTintColor = true;
        //更换颜色
        mImageTintInfo.mTintColor = mTintManager.getReplaceColorValue(mImageTintRes);
        //applySupportImageTint 换肤
        return applySupportImageTint();
    }

    private void setSupportImageTintMode(BlendMode mode) {
        if (mImageTintInfo == null) {
            mImageTintInfo = new TintInfo();
        }
        mImageTintInfo.mHasTintMode = true;
        mImageTintInfo.mTintMode = mode;
    }

    private boolean applySupportImageTint() {
        PixelMapElement image = new PixelMapElement(mView.getPixelMap());
        if (image != null && mImageTintInfo != null && mImageTintInfo.mHasTintColor) {
            if (mImageTintInfo.mHasTintColor) {
                //换肤
                ElementTintUtil.setColorTint(image, mImageTintInfo.mTintColor);
            }
            if (mImageTintInfo.mHasTintMode) {
                image.setStateColorMode(mImageTintInfo.mTintMode);
            }
            setImageDrawable(image);
            return true;
        }
        return false;
    }

    private void resetTintResource(Element element) {
        mElementRes = element;
        mImageTintRes = null;
        if (mImageTintInfo != null) {
            mImageTintInfo.mHasTintColor = false;
            mImageTintInfo.mTintColor = 0;
            mImageTintInfo.mHasTintMode = false;
            mImageTintInfo.mTintMode = null;
        }
    }

    @Override
    public void tint() {
        setSupportImageTint();
    }

    public interface ImageExtensible {
        void setImageTintList(Color color);

        void setImageTintList(Color color, BlendMode mode);
    }
}
