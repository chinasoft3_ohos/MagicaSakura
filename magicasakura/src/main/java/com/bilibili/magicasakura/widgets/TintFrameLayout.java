/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.bilibili.magicasakura.utils.TintManager;


/**
 * @author xyczero617@gmail.com
 * @time 16/2/14
 */
public class TintFrameLayout extends StackLayout implements Tintable, AppCompatBackgroundHelper.BackgroundExtensible,
        AppCompatForegroundHelper.ForegroundExtensible , Component.ComponentStateChangedListener {

    private AppCompatBackgroundHelper mBackgroundHelper;
    private AppCompatForegroundHelper mForegroundHelper;

    public TintFrameLayout(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public TintFrameLayout(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, null);
        TintManager tintManager = TintManager.get(context);

        mBackgroundHelper = new AppCompatBackgroundHelper(this, tintManager);
        mBackgroundHelper.loadFromAttribute(attrs, defStyleAttr);

        mForegroundHelper = new AppCompatForegroundHelper(this, tintManager);
        mForegroundHelper.loadFromAttribute(attrs, defStyleAttr);
    }

    @Override
    public void onComponentStateChanged(Component component, int i) {
        if (getBackgroundElement() != null) {
            invalidate();
        }
    }

    @Override
    public void setForeground(Element foreground) {
        super.setForeground(foreground);
        if (mForegroundHelper != null) {
            mForegroundHelper.setForegroundDrawableExternal(foreground);
        }
    }

    @Override
    public void setForegroundTintList(Color color) {
        if (mForegroundHelper != null) {
            mForegroundHelper.setForegroundTintList(color, null);
        }
    }

    @Override
    public void setForegroundTintList(Color resId, BlendMode mode) {
        if (mForegroundHelper != null) {
            mForegroundHelper.setForegroundTintList(resId, mode);
        }
    }

    @Override
    public void setBackground(Element element) {
        super.setBackground(element);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundDrawableExternal(element);
        }
    }

    public void setBackgroundRes(int resId) {
        ShapeElement shapeElement=new ShapeElement(mContext,resId);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundRes(shapeElement);
        } else {
            super.setBackground(shapeElement);
        }
    }


    @Override
    public void setBackgroundTintList(Color color) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color,BlendMode.SRC_IN);
        }
    }

    @Override
    public void setBackgroundTintList(Color color, BlendMode mode) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color,mode);
        }
    }

    @Override
    public void tint() {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.tint();
        }
        if (mForegroundHelper != null) {
            mForegroundHelper.tint();
        }
    }

}
