package com.bilibili.magicasakura.widgets;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Switch;
import ohos.agp.components.element.AnimatedStateElement;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.bilibili.magicasakura.utils.ThemeUtils;
import com.bilibili.magicasakura.utils.TintManager;

/**
 * @author xyczero617@gmail.com
 * @since 17/5/23
 */

public class TintSwitchCompat extends Switch implements Tintable, AppCompatTextHelper.TextExtensible,
    AppCompatBackgroundHelper.BackgroundExtensible, AppCompatSwitchHelper.SwitchCompatExtensible,
    AppCompatCompoundButtonHelper.CompoundButtonExtensible, Component.ComponentStateChangedListener {

    private AppCompatBackgroundHelper mBackgroundHelper;
    private AppCompatCompoundButtonHelper mCompoundButtonHelper;
    private AppCompatTextHelper mTextHelper;
    private AppCompatSwitchHelper mThumbHelper;
    private AppCompatSwitchHelper mTrackHelper;

    public TintSwitchCompat(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    public TintSwitchCompat(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TintManager tintManager = TintManager.get(context);

        String TintSwitchThumb = "thumb_element";
        mThumbHelper = new AppCompatSwitchHelper(this, tintManager,
            TintSwitchThumb,
            new AppCompatSwitchHelper.DrawableCallback() {
                @Override
                public void setDrawable(Element drawable) {
                    setThumbElement(drawable);
                }

                @Override
                public Element getDrawable() {
                    return getThumbElement();
                }
            });
        mThumbHelper.loadFromAttribute(attrs, 0);

        String TintSwitchTrack = "track_element";
        mTrackHelper = new AppCompatSwitchHelper(this, tintManager,
            TintSwitchTrack,
            new AppCompatSwitchHelper.DrawableCallback() {
                @Override
                public void setDrawable(Element drawable) {
                    setTrackElement(drawable);
                }

                @Override
                public Element getDrawable() {
                    return getThumbElement();
                }
            });
        mTrackHelper.loadFromAttribute(attrs, 0);

        mBackgroundHelper = new AppCompatBackgroundHelper(this, tintManager);
        mBackgroundHelper.loadFromAttribute(attrs, 0);

        mCompoundButtonHelper = new AppCompatCompoundButtonHelper(this, tintManager);
        mCompoundButtonHelper.loadFromAttribute(attrs, 0);

        mTextHelper = new AppCompatTextHelper(this, tintManager);
        mTextHelper.loadFromAttribute(attrs, 0);
    }

    @Override
    public void onComponentStateChanged(Component component, int i) {
    }

    @Override
    public void setThumbElement(Element element) {
        super.setThumbElement(element);
        if (mThumbHelper != null) {
            mThumbHelper.setDrawable();
        }
    }

    @Override
    public void setThumbTintList(Color color) {
        if (mThumbHelper != null) {
            mThumbHelper.setButtonDrawableTintList(color, null);
        }
    }

    @Override
    public void setThumbTintList(Color color, BlendMode mode) {
        if (mThumbHelper != null) {
            mThumbHelper.setButtonDrawableTintList(color, mode);
        }
    }

    @Override
    public void setTrackElement(Element element) {
        super.setTrackElement(element);
        if (mTrackHelper != null) {
            mTrackHelper.setDrawable();
        }
    }

    @Override
    public void setTrackTintList(Color color) {
        if (mTrackHelper != null) {
            mTrackHelper.setButtonDrawableTintList(color, null);
        }
    }

    @Override
    public void setTrackTintList(Color color, BlendMode mode) {
        if (mTrackHelper != null) {
            mTrackHelper.setButtonDrawableTintList(color, mode);
        }
    }

    @Override
    public void setTextColor(Color color) {
        super.setTextColor(color);
        if (mTextHelper != null) {
            mTextHelper.setTextColor();
        }
    }

    @Override
    public void setBackground(Element element) {
        super.setBackground(element);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundDrawableExternal(element);
        }
    }

    @Override
    public void setButtonElement(Element element) {
        super.setButtonElement(element);
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.setButtonDrawable();
        }
    }

    @Override
    public int getPaddingLeft() {
        final int value = super.getPaddingLeft();
        return mCompoundButtonHelper != null
            ? mCompoundButtonHelper.getCompoundPaddingLeft(value)
            : value;
    }

    @Override
    public void tint() {
        if (mTextHelper != null) {
            mTextHelper.tint();
        }
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.tint();
        }
        if (mBackgroundHelper != null) {
            mBackgroundHelper.tint();
        }
        if (mTrackHelper != null) {
            mTrackHelper.tint();
        }
        if (mThumbHelper != null) {
            mThumbHelper.tint();
        }
    }

    @Override
    public void setBackgroundTintList(Color color) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, BlendMode.SRC);
        }
    }

    @Override
    public void setBackgroundTintList(Color color, BlendMode mode) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, mode);
        }
    }

    @Override
    public void setCompoundButtonTintList(Color color) {
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.setButtonDrawableTintList(color, BlendMode.SRC);
        }
    }

    @Override
    public void setCompoundButtonTintList(Color color, BlendMode mode) {
        if (mCompoundButtonHelper != null) {
            mCompoundButtonHelper.setButtonDrawableTintList(color, mode);
        }
    }

    @Override
    public void setTextColorByColor(Color color) {
        if (mTextHelper != null) {
            mTextHelper.setTextColorByColor(color);
        }
    }
}
