/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentState;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;

import com.bilibili.magicasakura.utils.AttrValue;
import com.bilibili.magicasakura.utils.ElementTintUtil;
import com.bilibili.magicasakura.utils.ThemeUtils;
import com.bilibili.magicasakura.utils.TintInfo;
import com.bilibili.magicasakura.utils.TintManager;

/**
 * 输入框下划线颜色
 *
 * @since 2021-05-27
 */
public class AppCompatBasementHelper extends AppCompatBaseHelper<TextField> {
    private Color mBasemenColor;
    private Element mBasemenElement;

    private TintInfo mBasemenColorTintInfo;

    AppCompatBasementHelper(TextField view, TintManager tintManager) {
        super(view, tintManager);
    }

    @Override
    void loadFromAttribute(AttrSet attrs, int defStyleAttr) {
        if (attrs.getAttr("basementTint").isPresent()) {
            mBasemenColor = AttrValue.get(attrs, "basementTint", ThemeUtils.themeColor);
            setSupportBasementTint();
        } else {
            Element drawable = null;
            mBasemenElement = AttrValue.get(attrs, "basement", drawable);
        }
    }

    /**
     * External use
     *
     * @param background background
     */
    public void setBasementDrawableExternal(Element background) {
        if (skipNextApply()) return;

        resetTintResource(background);
        setSkipNextApply(false);
    }

    public void setBasementColor(int color) {
        if (skipNextApply()) return;
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(ThemeUtils.getColor(mView.getContext(), color)));
        resetTintResource(element);
        mView.setBasement(element);
    }

    public void setBasemen(Element element) {
        if (mBasemenElement != element) {
            resetTintResource(element);

            if (element != null) {
                setBasementDrawable(element);
            }
        }
    }

    public void setBasemenTintList(Color color, BlendMode mode) {
        if (mBasemenColor.getValue() != color.getValue()) {
            mBasemenColor = color;
            if (mBasemenColorTintInfo != null) {
                mBasemenColorTintInfo.mHasTintColor = false;
                mBasemenColorTintInfo.mTintColor = 0;
            }
            setSupportBasementTintMode(mode);
            setSupportBasementTint();
        }
    }

    /**
     * Internal use
     *
     * @param drawable drawable
     */
    private void setBasementDrawable(Element drawable) {
        if (skipNextApply()) return;
        mView.setBasement(drawable);
    }

    private boolean setSupportBasementTint() {
        if (mBasemenColorTintInfo == null) {
            mBasemenColorTintInfo = new TintInfo();
        }
        mBasemenColorTintInfo.mHasTintColor = true;
        mBasemenColorTintInfo.mTintColor = mTintManager.getReplaceColorValue(mBasemenColor);
        return applySupportBasementTint();
    }

    private void setSupportBasementTintMode(BlendMode mode) {
        if (mBasemenColor != null && mode != null) {
            if (mBasemenColorTintInfo == null) {
                mBasemenColorTintInfo = new TintInfo();
            }
            mBasemenColorTintInfo.mHasTintMode = true;
            mBasemenColorTintInfo.mTintMode = mode;
        }
    }

    private boolean applySupportBasementTint() {
        Element basementDrawable = mView.getBasement();
        if (basementDrawable != null && mBasemenColorTintInfo != null && mBasemenColorTintInfo.mHasTintColor) {
            if (basementDrawable instanceof StateElement) {
                StateElement compoundDrawable = (StateElement) basementDrawable;
                StateElement newElement = new StateElement();
                {
                    //聚焦状态下的图片
                    int[] states = {ComponentState.COMPONENT_STATE_FOCUSED};
                    int index = compoundDrawable.findStateElementIndex(states);
                    if (index >= 0) {
                        Element element = compoundDrawable.getStateElement(index);
                        if (element != null) {
                            if (element instanceof ShapeElement) {
                                ShapeElement element1 = new ShapeElement();
                                element1.setRgbColor(RgbColor.fromArgbInt(mBasemenColorTintInfo.mTintColor));
                                element = element1;
                            } else
                                ElementTintUtil.setColorTint(element, mBasemenColorTintInfo.mTintColor);
                        }
                        newElement.addState(states, element);
                    }
                }
                {
                    //正常状态下的图片
                    int[] selectStates = {ComponentState.COMPONENT_STATE_EMPTY};
                    int selectIndex = compoundDrawable.findStateElementIndex(selectStates);
                    if (selectIndex >= 0) {
                        Element element = compoundDrawable.getStateElement(selectIndex);
                        newElement.addState(selectStates, element);
                    }
                }
                setBasementDrawable(newElement);
            } else {
                ElementTintUtil.setColorTint(basementDrawable, mBasemenColorTintInfo.mTintColor);
                setBasementDrawable(basementDrawable);
            }
            return true;
        }
        return false;
    }

    private void resetTintResource(Element element) {
        mBasemenElement = element;
        mBasemenColor = null;
        if (mBasemenColorTintInfo != null) {
            mBasemenColorTintInfo.mHasTintColor = false;
            mBasemenColorTintInfo.mTintColor = 0;
            mBasemenColorTintInfo.mHasTintMode = false;
            mBasemenColorTintInfo.mTintMode = null;
        }
    }

    @Override
    public void tint() {
        if (mBasemenColor != null) {
            setSupportBasementTint();
        }
    }

    public interface BasementExtensible {
        void setBasementTintList(Color color);

        void setBasementTintList(Color color, BlendMode mode);
    }
}
