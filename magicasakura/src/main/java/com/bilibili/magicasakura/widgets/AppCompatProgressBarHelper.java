/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.RoundProgressBar;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

import com.bilibili.magicasakura.utils.ElementTintUtil;
import com.bilibili.magicasakura.utils.TintInfo;
import com.bilibili.magicasakura.utils.TintManager;

/**
 * @author xyczero617@gmail.com
 * @time 16/2/4
 */
class AppCompatProgressBarHelper extends AppCompatBaseHelper<ProgressBar> {

    private Color mProgressTintRes;
    private Color mIndeterminateTintRes;

    private TintInfo mProgressTintInfo;
    private TintInfo mIndeterminateTintInfo;

    AppCompatProgressBarHelper(ProgressBar view, TintManager tintManager) {
        super(view, tintManager);
    }

    @SuppressWarnings("ResourceType")
    @Override
    void loadFromAttribute(AttrSet attrs, int defStyleAttr) {
        if (attrs.getAttr("progressTint").isPresent()) {
            mProgressTintRes = attrs.getAttr("progressTint").get().getColorValue();
            setSupportProgressTint(mProgressTintRes);
        }
        if (attrs.getAttr("progressIndeterminateTint").isPresent()) {
            mIndeterminateTintRes = attrs.getAttr("progressIndeterminateTint").get().getColorValue();
            setSupportIndeterminateTint(mIndeterminateTintRes);
        }
    }

    private void setSupportProgressTint(Color color) {
        if (color != null) {
            if (mProgressTintInfo == null) {
                mProgressTintInfo = new TintInfo();
            }
            mProgressTintInfo.mHasTintColor = true;
            mProgressTintInfo.mTintColor = mTintManager.getReplaceColorValue(color);
        }
        applySupportProgressTint();
    }

    private void setSupportIndeterminateTint(Color color) {
        if (color != null) {
            if (mIndeterminateTintInfo == null) {
                mIndeterminateTintInfo = new TintInfo();
            }
            mIndeterminateTintInfo.mHasTintColor = true;
            mIndeterminateTintInfo.mTintColor = mTintManager.getReplaceColorValue(color);
        }
        applySupportIndeterminateTint();
    }

    private void applySupportProgressTint() {
        final Element target = getTintTarget(true);
        if (target != null && mProgressTintInfo != null && mProgressTintInfo.mHasTintColor) {
            //换肤
            ElementTintUtil.setColorTint(target, mProgressTintInfo.mTintColor);

        }
    }

    private void applySupportIndeterminateTint() {
        if (mIndeterminateTintInfo != null
            && mIndeterminateTintInfo.mHasTintColor) {
            setProgressElement(new Color(mIndeterminateTintInfo.mTintColor));
        }
    }

    private Element getTintTarget(boolean shouldFallback) {
        Element layer = null;
        final Element d = mView.getProgressElement();
        if (d != null) {
            mView.setProgressElement(d);

            if (shouldFallback && layer == null) {
                layer = d;
            }
        }

        return layer;
    }

    public void setProgressElement() {
        if (skipNextApply()) return;

        resetTintResource(null);
        setSkipNextApply(false);
    }

    private void setProgressElement(Color color) {
        if (skipNextApply()) return;
        if (mView instanceof RoundProgressBar) {
            mView.setProgressColor(color);
        } else {
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(color.getValue()));
            if (mView.isInfiniteMode()) {
                mView.setProgressElement(element);
                mView.setInfiniteModeElement(element);
            } else {
                mView.setProgressElement(element);
                ShapeElement element1 = new ShapeElement();
                element1.setRgbColor(RgbColor.fromArgbInt(color.getValue()));
                element1.setAlpha(80);
                mView.setProgressBackgroundElement(element1);
            }
        }
    }

    private void resetTintResource(Element element) {
        mIndeterminateTintRes = null;
        if (mIndeterminateTintInfo != null) {
            mIndeterminateTintInfo.mHasTintColor = false;
            mIndeterminateTintInfo.mTintColor = 0;
            mIndeterminateTintInfo.mHasTintMode = false;
            mIndeterminateTintInfo.mTintMode = null;
        }
    }

    @Override
    public void tint() {
        if (mProgressTintRes != null) {
            setSupportProgressTint(mProgressTintRes);
        }
        if (mIndeterminateTintRes != null) {
            setSupportIndeterminateTint(mIndeterminateTintRes);
        }
    }
}
