/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;


import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.bilibili.magicasakura.utils.TintManager;

/**
 * @author xyczero617@gmail.com
 * @time 15/10/25
 */

public class TintButton extends Button implements Tintable, AppCompatBackgroundHelper.BackgroundExtensible,
    AppCompatTextHelper.TextExtensible, Component.ComponentStateChangedListener {
    private AppCompatTextHelper mTextHelper;
    private AppCompatBackgroundHelper mBackgroundHelper;

    public TintButton(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public TintButton(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, null);
        TintManager tintManager = TintManager.get(context);

        mBackgroundHelper = new AppCompatBackgroundHelper(this, tintManager);
        mBackgroundHelper.loadFromAttribute(attrs, defStyleAttr);

        mTextHelper = new AppCompatTextHelper(this, tintManager);
        mTextHelper.loadFromAttribute(attrs, defStyleAttr);
    }

    @Override
    public void onComponentStateChanged(Component component, int i) {
    }

    @Override
    public void setTextColor(Color color) {
        super.setTextColor(color);
        if (mTextHelper != null) {
            mTextHelper.setTextColor();
        }
    }

    @Override
    public void setBackground(Element element) {
        super.setBackground(element);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundDrawableExternal(element);
        }
    }

    public void setBackgroundResource(int resId) {
        ShapeElement shapeElement = new ShapeElement(mContext, resId);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundRes(shapeElement);
        } else {
            super.setBackground(shapeElement);
        }
    }

    public void setBackgroundColor(int color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        super.setBackground(shapeElement);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundColor(color);
        }
    }

    @Override
    public void setBackgroundTintList(Color color) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, BlendMode.SRC_IN);
        }
    }

    @Override
    public void setBackgroundTintList(Color color, BlendMode mode) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, mode);
        }
    }

    @Override
    public void setTextColorByColor(Color color) {
        if (mTextHelper != null) {
            mTextHelper.setTextColorByColor(color);
        }
    }

    @Override
    public void tint() {
        if (mTextHelper != null) {
            mTextHelper.tint();
        }
        if (mBackgroundHelper != null) {
            mBackgroundHelper.tint();

        }
    }
}