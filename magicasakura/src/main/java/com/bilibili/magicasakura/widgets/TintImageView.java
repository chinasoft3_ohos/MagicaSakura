/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import com.bilibili.magicasakura.utils.TintManager;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author xyczero617@gmail.com
 * @time 15/11/8
 */
public class TintImageView extends Image implements Tintable, AppCompatBackgroundHelper.BackgroundExtensible,
    AppCompatImageHelper.ImageExtensible, Component.ComponentStateChangedListener {
    private AppCompatBackgroundHelper mBackgroundHelper;
    private AppCompatImageHelper mImageHelper;

    public TintImageView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public TintImageView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, null);
        TintManager tintManager = TintManager.get(context);

        mBackgroundHelper = new AppCompatBackgroundHelper(this, tintManager);
        mBackgroundHelper.loadFromAttribute(attrs, defStyleAttr);

        mImageHelper = new AppCompatImageHelper(this, tintManager);
        mImageHelper.loadFromAttribute(attrs, defStyleAttr);
    }

    @Override
    public void onComponentStateChanged(Component component, int i) {
        if (getBackgroundElement() != null) {
            invalidate();
        }
    }

    @Override
    public void setBackground(Element element) {
        super.setBackground(element);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundDrawableExternal(element);
        }
    }

    public void setBackgroundResource(int resId) {
        ShapeElement shapeElement = new ShapeElement(mContext, resId);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundRes(shapeElement);
        } else {
            super.setBackground(shapeElement);
        }
    }

    public void setBackgroundColor(int color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        super.setBackground(shapeElement);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundColor(color);
        }
    }

    @Override
    public void setPixelMap(int resId) {
        if (mImageHelper != null) {
            PixelMapElement image = new PixelMapElement(getPixelMapFromResource(mContext, resId));
            mImageHelper.setImageRes(image);
        } else {
            super.setPixelMap(resId);
        }
    }

    @Override
    public void setPixelMap(PixelMap pixelMap) {
        if (mImageHelper != null) {
            PixelMapElement image = new PixelMapElement(pixelMap);
            mImageHelper.setImageRes(image);
        } else {
            super.setPixelMap(pixelMap);
        }
    }

    @Override
    public void setImageElement(Element element) {
        super.setImageElement(element);
        if (mImageHelper != null) {
            mImageHelper.setImageDrawable();
        }
    }

    public void setImageResource(int resId) {
        ShapeElement shapeElement = new ShapeElement(mContext, resId);
        if (mImageHelper != null) {
            mImageHelper.setImageRes(shapeElement);
        } else {
            super.setImageElement(shapeElement);
        }
    }


    @Override
    public void setBackgroundTintList(Color color) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, BlendMode.SRC_IN);
        }
    }

    @Override
    public void setBackgroundTintList(Color color, BlendMode mode) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, mode);
        }
    }

    @Override
    public void setImageTintList(Color color) {
        if (mImageHelper != null) {
            mImageHelper.setImageTintList(color, BlendMode.SRC_IN);
        }
    }

    @Override
    public void setImageTintList(Color resId, BlendMode mode) {
        if (mImageHelper != null) {
            mImageHelper.setImageTintList(resId, mode);
        }
    }

    @Override
    public void tint() {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.tint();
        }
        if (mImageHelper != null) {
            mImageHelper.tint();
        }
    }

    public static PixelMap getPixelMapFromResource(Context context, int resourceId) {
        InputStream inputStream = null;
        ImageSource imageSource = null;
        ImageSource.DecodingOptions decodingOptions = null;
        try {
            // 创建图像数据源ImageSource对象
            inputStream = context.getResourceManager().getResource(resourceId);
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/jpg";
            imageSource = ImageSource.create(inputStream, srcOpts);

            // 设置图片参数
            decodingOptions = new ImageSource.DecodingOptions();
            return imageSource.createPixelmap(decodingOptions);
        } catch (IOException e) {
        } catch (NotExistException e) {
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }
}
