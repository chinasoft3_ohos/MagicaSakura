/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;


import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.bilibili.magicasakura.utils.TintManager;

public class TintEditText extends TextField implements Tintable, AppCompatBackgroundHelper.BackgroundExtensible,
    AppCompatCompoundDrawableHelper.CompoundDrawableExtensible, AppCompatTextHelper.TextExtensible,
    AppCompatBasementHelper.BasementExtensible, Component.ComponentStateChangedListener {
    private AppCompatBackgroundHelper mBackgroundHelper;
    private AppCompatCompoundDrawableHelper mCompoundDrawableHelper;
    private AppCompatTextHelper mTextHelper;
    private AppCompatBasementHelper mBasementHelper;

    public TintEditText(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    public TintEditText(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setComponentStateChangedListener(this);
        TintManager tintManager = TintManager.get(getContext());

        mTextHelper = new AppCompatTextHelper(this, tintManager);
        mTextHelper.loadFromAttribute(attrs, 0);

        mBackgroundHelper = new AppCompatBackgroundHelper(this, tintManager);
        mBackgroundHelper.loadFromAttribute(attrs, 0);

        mCompoundDrawableHelper = new AppCompatCompoundDrawableHelper(this, tintManager);
        mCompoundDrawableHelper.loadFromAttribute(attrs, 0);

        mBasementHelper = new AppCompatBasementHelper(this, tintManager);
        mBasementHelper.loadFromAttribute(attrs, 0);
    }

    @Override
    public void onComponentStateChanged(Component component, int i) {
        if (getBackgroundElement() != null) {
            invalidate();
        }
    }

    @Override
    public void setTextColor(Color color) {
        super.setTextColor(color);
        if (mTextHelper != null) {
            mTextHelper.setTextColor();
        }
    }

    @Override
    public void setBackground(Element background) {
        super.setBackground(background);
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundDrawableExternal(background);
        }
    }

    public void setBackgroundColor(int color) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundColor(color);
        } else {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
            super.setBackground(shapeElement);
        }
    }

    @Override
    public void setAroundElements(Element left, Element top, Element right, Element bottom) {
        super.setAroundElements(left, top, right, bottom);
        if (mCompoundDrawableHelper != null) {
            mCompoundDrawableHelper.setCompoundDrawablesWithIntrinsicBounds();
        }
    }

    @Override
    public void setBasement(Element element) {
        super.setBasement(element);
        if (mBasementHelper != null) {
            mBasementHelper.setBasementDrawableExternal(element);
        }
    }

    @Override
    public void tint() {
        if (mTextHelper != null) {
            mTextHelper.tint();
        }
        if (mBackgroundHelper != null) {
            mBackgroundHelper.tint();
        }
        if (mCompoundDrawableHelper != null) {
            mCompoundDrawableHelper.tint();
        }
        if (mBasementHelper != null) {
            mBasementHelper.tint();
        }
    }

    @Override
    public void setBackgroundTintList(Color color) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, BlendMode.SRC_IN);
        }
    }

    @Override
    public void setBackgroundTintList(Color color, BlendMode mode) {
        if (mBackgroundHelper != null) {
            mBackgroundHelper.setBackgroundTintList(color, mode);
        }
    }

    @Override
    public void setTextColorByColor(Color color) {
        if (mTextHelper != null) {
            mTextHelper.setTextColorByColor(color);
        }
    }

    @Override
    public void setCompoundDrawableTintList(Color... colors) {
        if (mCompoundDrawableHelper != null) {
            mCompoundDrawableHelper.setCompoundDrawablesTintList(colors);
        }
    }

    @Override
    public void setBasementTintList(Color color) {
        if (mBasementHelper != null) {
            mBasementHelper.setBasemenTintList(color, null);
        }
    }

    @Override
    public void setBasementTintList(Color color, BlendMode mode) {
        if (mBasementHelper != null) {
            mBasementHelper.setBasemenTintList(color, mode);
        }
    }
}
