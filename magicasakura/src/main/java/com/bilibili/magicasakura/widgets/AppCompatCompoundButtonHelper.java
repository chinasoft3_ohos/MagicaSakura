/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.widgets;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AbsButton;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentState;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;

import com.bilibili.magicasakura.utils.AttrValue;
import com.bilibili.magicasakura.utils.DrawableUtils;
import com.bilibili.magicasakura.utils.ElementTintUtil;
import com.bilibili.magicasakura.utils.ThemeUtils;
import com.bilibili.magicasakura.utils.TintInfo;
import com.bilibili.magicasakura.utils.TintManager;

/**
 * @author xyczero617@gmail.com
 * @time 15/11/23
 */
class AppCompatCompoundButtonHelper extends AppCompatBaseHelper<AbsButton> {

    private TintInfo mCompoundButtonTintInfo;
    private Element mCompoundButtonRes;
    private Color mCompoundButtonTintRes;

    AppCompatCompoundButtonHelper(AbsButton view, TintManager tintManager) {
        super(view, tintManager);
    }

    @SuppressWarnings("ResourceType")
    @Override
    void loadFromAttribute(AttrSet attrs, int defStyleAttr) {
        if (attrs.getAttr("compoundButtonTint").isPresent()) {
            mCompoundButtonTintRes = AttrValue.get(attrs, "compoundButtonTint", ThemeUtils.themeColor);
            if (attrs.getAttr("compoundButtonTintMode").isPresent()) {
                setSupportButtonDrawableTintMode(DrawableUtils.parseTintMode(AttrValue.get(attrs, "compoundButtonTintMode", 0), null));
            }
            setSupportButtonDrawableTint();
        } else {
            Element element = null;
            mCompoundButtonRes = AttrValue.get(attrs, "button", element);
            if (mCompoundButtonRes != null) {
                setButtonDrawable(mCompoundButtonRes);
            }
        }


    }

    /**
     * External use
     */
    public void setButtonDrawable() {
        if (skipNextApply()) return;

        resetTintResource(null);
        setSkipNextApply(false);
    }

    public void setButtonDrawable(Color color) {
        if (mCompoundButtonTintRes.getValue() != color.getValue()) {
            ShapeElement drawable = new ShapeElement();
            drawable.setRgbColor(RgbColor.fromArgbInt(color.getValue()));
            resetTintResource(drawable);
            setButtonDrawable(drawable);
        }
    }

    public void setButtonDrawableTintList(Color color, BlendMode mode) {
        if (mCompoundButtonTintRes != color) {
            mCompoundButtonTintRes = color;
            if (mCompoundButtonTintInfo != null) {
                mCompoundButtonTintInfo.mHasTintColor = false;
                mCompoundButtonTintInfo.mTintColor = 0;
                mCompoundButtonTintInfo.mHasTintMode = false;
                mCompoundButtonTintInfo.mTintMode = null;
            }
            setSupportButtonDrawableTintMode(mode);
            setSupportButtonDrawableTint();
        }
    }

    /**
     * Internal use
     *
     * @param drawable drawable
     */
    private void setButtonDrawable(Element drawable) {
        if (skipNextApply()) return;

        mView.setButtonElement(drawable);
    }

    public boolean setSupportButtonDrawableTint() {
        if (mCompoundButtonTintInfo == null) {
            mCompoundButtonTintInfo = new TintInfo();
        }
        mCompoundButtonTintInfo.mHasTintColor = true;
        mCompoundButtonTintInfo.mTintColor = mTintManager.getReplaceColorValue(mCompoundButtonTintRes);
        return applySupportButtonDrawableTint();
    }

    private void setSupportButtonDrawableTintMode(BlendMode mode) {
        if (mCompoundButtonTintRes != null && mode != null) {
            if (mCompoundButtonTintInfo == null) {
                mCompoundButtonTintInfo = new TintInfo();
            }
            mCompoundButtonTintInfo.mHasTintMode = true;
            mCompoundButtonTintInfo.mTintMode = mode;
        }
    }

    public boolean applySupportButtonDrawableTint() {
        if (mCompoundButtonTintInfo != null && mCompoundButtonTintInfo.mHasTintColor) {
            int[] states = {ComponentState.COMPONENT_STATE_CHECKED};
            int[] states1 = {ComponentState.COMPONENT_STATE_EMPTY};
            ShapeElement element = new ShapeElement();
            ShapeElement element1 = new ShapeElement();
            element.setShape(ShapeElement.OVAL);
            element.setRgbColor(RgbColor.fromArgbInt(mCompoundButtonTintInfo.mTintColor));
            element1.setShape(ShapeElement.OVAL);
            element1.setRgbColor(RgbColor.fromArgbInt(Color.GRAY.getValue()));

            StateElement stateElement = new StateElement();
            stateElement.addState(states, element);
            stateElement.addState(states1, element1);
            setButtonDrawable(stateElement);
            return true;
        }

        return false;
    }

    private void resetTintResource(Element element) {
        mCompoundButtonRes = element;
        mCompoundButtonTintRes = null;
        if (mCompoundButtonTintInfo != null) {
            mCompoundButtonTintInfo.mHasTintList = false;
            mCompoundButtonTintInfo.mTintList = null;
        }
    }

    public int getCompoundPaddingLeft(int superValue) {
        Element buttonDrawable = mView.getButtonElement();
        if (buttonDrawable != null) {
            superValue += buttonDrawable.getWidth();
        }
        return superValue;
    }

    @Override
    public void tint() {
        setSupportButtonDrawableTint();
    }

    public interface CompoundButtonExtensible {
        void setCompoundButtonTintList(Color color);

        void setCompoundButtonTintList(Color color, BlendMode mode);
    }
}
