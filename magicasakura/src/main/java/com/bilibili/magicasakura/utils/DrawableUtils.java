/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.utils;

import ohos.agp.render.BlendMode;

/**
 * 工具类
 *
 * @author xyczero617@gmail.com
 * @time 16/2/22
 */
public final class DrawableUtils {
    /**
     * BlendMode
     *
     * @param value value
     * @param defaultMode defaultMode
     * @return BlendMode
     */
    public static BlendMode parseTintMode(int value, BlendMode defaultMode) {
        switch (value) {
            case 3:
                return BlendMode.SRC_OVER;
            case 5:
                return BlendMode.SRC_IN;
            case 9:
                return BlendMode.SRC_ATOP;
            case 14:
                return BlendMode.MULTIPLY;
            case 15:
                return BlendMode.SCREEN;
            case 16:
                return BlendMode.valueOf("ADD");
            default:
                return defaultMode;
        }
    }
}
