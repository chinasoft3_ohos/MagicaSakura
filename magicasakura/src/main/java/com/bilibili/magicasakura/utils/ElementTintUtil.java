/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.utils;

import ohos.agp.components.element.Element;
import ohos.agp.render.ColorMatrix;
import ohos.agp.utils.Color;

/**
 * element颜色修改工具类
 *
 * @since 2021-05-21
 */
public class ElementTintUtil {
    private ElementTintUtil() {
    }

    /**
     * 更改图片颜色
     *
     * @param element
     * @param maskColor
     */
    public static void setColorTint(Element element, int maskColor) {
        final float num1 = 0.15f;
        final float num2 = 255f;
        final int num3 = 16;
        final int num4 = 8;
        final int ff = 0xFF;

        // 忽略透明度时的颜色矩阵
        float r1 = Color.alpha(maskColor) / num2;
        r1 = r1 - (1 - r1) * num1;
        float rr = (1 - r1) * num1;
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setMatrix(new float[]{
            rr, 0, 0, 0, ((maskColor >> num3) & ff) * r1,
            0, rr, 0, 0, ((maskColor >> num4) & ff) * r1,
            0, 0, rr, 0, (maskColor & ff) * r1,
            0, 0, 0, 1, 0,
        });
        element.setColorMatrix(colorMatrix);
    }
}
