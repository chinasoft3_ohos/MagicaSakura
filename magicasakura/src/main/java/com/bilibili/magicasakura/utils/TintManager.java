/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakura.utils;

import ohos.agp.components.element.ElementContainer;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Logger;


/**
 * @author xyczero617@gmail.com
 * @time 15/9/15
 */
public final class TintManager {
    private static final String TAG = "TintManager";
    private static final boolean DEBUG = false;
    private static final BlendMode DEFAULT_MODE = BlendMode.SRC_IN;
    private static final String SKIP_DRAWABLE_TAG = "appcompat_skip_skip";

    private static final WeakHashMap<Context, TintManager> INSTANCE_CACHE = new WeakHashMap<>();

    private final Object mDrawableCacheLock = new Object();

    private WeakReference<Context> mContextRef;
    private HashMap<Integer, StateElement> mCacheTintList;
    private HashMap<Integer, WeakReference<ElementContainer.ElementState>> mCacheDrawables;
    private HashMap<Integer, String> mSkipDrawableIdTags;

    public static TintManager get(Context context) {
        if (context == null) return null;
        TintManager tm = INSTANCE_CACHE.get(context);
        if (tm == null) {
            tm = new TintManager(context);
            INSTANCE_CACHE.put(context, tm);
            printLog("[get TintManager] create new TintManager.");
        }
        return tm;
    }

    private TintManager(Context context) {
        mContextRef = new WeakReference<>(context);
    }

    public static void clearTintCache() {
        for (Map.Entry<Context, TintManager> entry : INSTANCE_CACHE.entrySet()) {
            TintManager tm = entry.getValue();
            if (tm != null)
                tm.clear();
        }
    }

    private void clear() {
        if (mCacheTintList != null) {
            mCacheTintList.clear();
        }
        if (mCacheDrawables != null) {
            mCacheDrawables.clear();
        }
        if (mSkipDrawableIdTags != null) {
            mSkipDrawableIdTags.clear();
        }
    }

    public int getColorValue() {
        final Context context = mContextRef.get();
        if (context == null) {
            return 0;
        }

        int color = ThemeUtils.replaceColorById(context, ThemeUtils.themeColor);
        return color;
    }

    public int getReplaceColorValue(Color color) {
        final Context context = mContextRef.get();
        if (context == null || color == null) return 0;

        int colorValue = ThemeUtils.replaceColor(context, color.getValue());
        return colorValue;
    }

    private static void printLog(String msg) {
        if (DEBUG) {
            Logger.getGlobal().warning(TAG + msg);
        }
    }
}
