# MagicaSakura
#### 项目介绍

- 项目名称：MagicaSakura
- 所属系列：openharmony 第三方组件适配移植
- 功能：MagicaSakura是一个openharmony多主题库，支持每日色彩主题和夜间主题
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Releases 0.1.8

#### 效果演示
![sc1](screenshot/demo.gif)

#### 安装教程

1.在项目根目录下的build.gradle文件中，

```JAVA
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```JAVA
dependencies {
   implementation('com.gitee.chinasoft_ohos:MagicaSakura:1.0.2')
   ......  
}
```
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


-----------------------------------------------------------------------------------------------------

#### 使用说明
  
 - **步骤1** : 
 
 在element/color.json中定义你的应用全局主题颜色变量，如:
 ```json
 {
   "color": [
     {
       "name": "theme_color_primary",
       "value": "#fb7299"
     },
     {
       "name": "theme_color_primary_dark",
       "value": "#b85671"
     },
     {
       "name": "theme_color_primary_trans",
       "value": "#99f0486c"
     }]
}
 ```
必须使用这些颜色变量在布局xml中，这些xml文件需要自动适应不同的主题样式。如果你使用直接的颜色值或其他颜色变量，将失去适应不同的主题风格功能
 
 - **步骤2** :
 
 实现ThemeUtils。在应用程序中切换颜色界面;你可以结合颜色变量定义自己的规则(在步骤1中定义)，在选择不同主题时切换不同的颜色。
 ```java
public class MyApplication extends AbilityPackage implements ThemeUtils.switchColor {
     @Override
        public void onInitialize() {
            super.onInitialize();
            ThemeUtils.setSwitchColor(this);
        }
      
    @Override
    public int replaceColorById(Context context, Color color) {
        if (ThemeHelper.isDefaultTheme(context)) {
            return getColorValue(ResourceTable.Color_theme_color_primary);
        }
        int theme = getThemeColorId(context);
        return getColorValue(theme);
    }

    @Override
    public int replaceColor(Context context, int colorValue) {
        if (ThemeHelper.isDefaultTheme(context)) {
            return colorValue;
        }
       ...
    }
      
 }
 ```
 
 - **步骤3** :
 
 这个库提供了一系列Tintxxx小控件，其中包括最常见的ohos小控件。
 
 当你的应用中的某些地方需要适配多个主题时，你可以使用这些Tintxxx widgets **结合颜色变量(在步骤1中定义)或者颜色json(使用颜色变量)或绘制xml(颜色变量)**，那么它们将自动适应。
 
  - 绘制 Xml
    Tintxxx widgets支持常见的绘制 xml标签，如<state-container/>， <item/>, <shape/>等。
    
    (注意:当使用不支持绘制的xml标签时，就不能适应多主题)
  
    下面是一个例子:

    ``` java
    //color.json
    //tint directly
    <state-container
        xmlns:ohos="http://schemas.huawei.com/res/ohos">
    
    
        <item
            ohos:element="$color:theme_color_primary_dark"
            ohos:state="component_state_pressed" />
    
        <item
            ohos:element="$color:theme_color_primary"
            ohos:state="component_state_empty" />
    </state-container>
    
    ```
	
    ```java
    // set image 
    <state-container
        xmlns:ohos="http://schemas.huawei.com/res/ohos">
    
        <item
            ohos:element="$media:ic_lock_outline_white_24dp"
            ohos:state="component_state_focused" />
    
        <item
            ohos:element="$media:ic_lock_outline_white_24dp"
            ohos:state="component_state_empty" />
    </state-container>
    ```
 
  - Layout Xml
    Tintxxx控件可以直接在布局xml中着色，支持最常见的openharmony可绘制attrSet，如background, src, drawableLeft, button等。
    
    (注意:在布局xml中直接着色时，必须使用颜色变量(在步骤1中定义)，否则适应不同的主题样式将不起作用)
    
    下面是一个例子:
    
    ```java
    // selector_lock.xml
    <state-container
        xmlns:ohos="http://schemas.huawei.com/res/ohos">
    
        <item ohos:element="$media:ic_lock_open_white_24dp"
              ohos:state="component_state_pressed"/>
    
        <item ohos:element="$media:ic_lock_white_24dp"
              ohos:state="component_state_empty"/>
   
    </state-container>
    
    // TintTextView
    // The selector_lock and selector_text is a ColorStateList
     <com.bilibili.magicasakura.widgets.TintTextView
             ohos:id="$+id:title"
             ohos:height="match_content"
             ohos:width="match_content"
             ohos:clickable="true"
             ohos:element_left="$graphic:selector_lock"
             ohos:element_padding="8vp"
             ohos:text="$string:textview_title_unlock"
             ohos:text_alignment="vertical_center"
             ohos:text_color="#111111"
             ohos:text_size="19fp"
             app:drawableLeftStateTint="$color:theme_color_primary"
             app:drawableLeftTint="$color:gray_dark" />
    ```
    
    下面是支持Tintxxx控件attr的表格:
    
    | attr     | tint | 
    | :------- | ----: |
    | background | backgroundTint |  
    | src    | imageTint   | 
    | button     | compoundButtonTint    | 
    | drawableXxx     | drawableXxxTint    |  
    | progress     | progressTint,progressIndeterminateTint    |   
    | track     | trackTint    |  
    | thumb     | thumbTint    |

  - Java code
    Java代码Tintxxx小控件也可以在java代码中着色。着色绘制的方式与ohos原生方法相同。
   
    下面是一个例子:

    ```java
    tintTextView.setBackgroundResource(ResourceTable.Graphic_selector_shape_lock);
    
    tintImageView.setImageResource(ResourceTable.Graphic_selecor_png_lock);
    tintImageView.setImageTintList(ResourceTable.Color_selector_color_lock);
    ```
    
- **步骤4** :
  
    库提供实用工具类Themeutils来满足一些特殊需求或您自己的定制小部件。
    
    实用程序类Themeltils主要提供了着色绘制和转换颜色变量(在步骤1中定义)的方法，当前的主题color.
    
    ```java
    ThemeUtils.replaceColorById(context, ResourceTable.Color_selector_color.lock);
    ```
  
#### 测试信息
CodeCheck代码测试无异常  
CloudTest代码测试无异常  
病毒安全检测通过  
当前版本demo功能与原组件基本无差异  

#### 版本迭代
- 1.0.2  

#### 版权和许可信息
```
Copyright 2016 Bilibili

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```