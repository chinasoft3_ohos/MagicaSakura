/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakurademo;

import ohos.aafwk.ability.AbilityPackage;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import com.bilibili.magicasakura.utils.ThemeUtils;
import com.bilibili.magicasakurademo.slice.utils.ThemeHelper;

import java.io.IOException;

/**
 * MyApplication
 *
 * @since 2021-05-21
 */
public class MyApplication extends AbilityPackage implements ThemeUtils.switchColor {
    @Override
    public void onInitialize() {
        super.onInitialize();
        ThemeUtils.setSwitchColor(this);
    }

    private int getThemeColorId(Context context) {
        if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_STORM) {
            return ResourceTable.Color_blue;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_HOPE) {
            return ResourceTable.Color_purple;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_WOOD) {
            return ResourceTable.Color_green;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_LIGHT) {
            return ResourceTable.Color_green_light;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_THUNDER) {
            return ResourceTable.Color_yellow;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_SAND) {
            return ResourceTable.Color_orange;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_FIREY) {
            return ResourceTable.Color_red;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_SAKURA) {
            return ResourceTable.Color_theme_color_primary;
        }
        return ResourceTable.Color_theme_color_primary;
    }

    private int getThemeDarkColor(Context context) {
        if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_STORM) {
            return ResourceTable.Color_blue_dark;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_HOPE) {
            return ResourceTable.Color_purple_dark;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_WOOD) {
            return ResourceTable.Color_green_dark;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_LIGHT) {
            return ResourceTable.Color_green_light_dark;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_THUNDER) {
            return ResourceTable.Color_yellow_dark;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_SAND) {
            return ResourceTable.Color_orange_dark;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_FIREY) {
            return ResourceTable.Color_red_dark;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_SAKURA) {
            return ResourceTable.Color_theme_color_primary_dark;
        }
        return ResourceTable.Color_theme_color_primary_dark;
    }

    private int getThemeTransColor(Context context) {
        if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_STORM) {
            return ResourceTable.Color_blue_trans;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_HOPE) {
            return ResourceTable.Color_purple_trans;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_WOOD) {
            return ResourceTable.Color_green_trans;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_LIGHT) {
            return ResourceTable.Color_green_light_trans;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_THUNDER) {
            return ResourceTable.Color_yellow_trans;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_SAND) {
            return ResourceTable.Color_orange_trans;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_FIREY) {
            return ResourceTable.Color_red_trans;
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_SAKURA) {
            return ResourceTable.Color_theme_color_primary_trans;
        }
        return ResourceTable.Color_theme_color_primary_trans;
    }

    /**
     * 获取主题色
     *
     * @param context context
     * @param colorValue theme
     * @param theme theme
     * @return int
     */
    private int getThemeColor(Context context, int colorValue, String theme) {
        switch (colorValue) {
            case 0xfffb7299:
                return getThemeColorId(context);
            case 0xffb85671:
                return getThemeDarkColor(context);
            case 0x99f0486c:
                return getThemeTransColor(context);
        }
        return -1;
    }

    private String getTheme(Context context) {
        if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_STORM) {
            return "blue";
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_HOPE) {
            return "purple";
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_WOOD) {
            return "green";
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_LIGHT) {
            return "green_light";
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_THUNDER) {
            return "yellow";
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_SAND) {
            return "orange";
        } else if (ThemeHelper.getTheme(context) == ThemeHelper.CARD_FIREY) {
            return "red";
        }
        return null;
    }

    @Override
    public int replaceColorById(Context context, Color color) {
        if (ThemeHelper.isDefaultTheme(context)) {
            return getColorValue(ResourceTable.Color_theme_color_primary);
        }
        int theme = getThemeColorId(context);
        return getColorValue(theme);
    }

    @Override
    public int replaceColor(Context context, int colorValue) {
        if (ThemeHelper.isDefaultTheme(context)) {
            return colorValue;
        }
        String theme = getTheme(context);
        int colorId = -1;
        if (theme != null) {
            colorId = getThemeColor(context, colorValue, theme);
        }
        return colorId != -1 ? getColorValue(colorId) : colorValue;
    }

    private int getColorValue(int colorRes) {
        int colorV = -1;
        try {
            colorV = getResourceManager().getElement(colorRes).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return colorV;
    }
}
