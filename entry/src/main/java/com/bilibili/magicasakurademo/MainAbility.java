package com.bilibili.magicasakurademo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.service.WindowManager;

import com.bilibili.magicasakurademo.slice.MainAbilitySlice;

/**
 * MainAbility
 *
 * @since 2021-05-21
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        WindowManager.getInstance().getTopWindow().get().setStatusBarVisibility(Component.HIDE);
    }
}
