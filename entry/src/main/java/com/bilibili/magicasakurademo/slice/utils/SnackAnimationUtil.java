/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakurademo.slice.utils;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * @author xyczero617@gmail.com
 * @time 16/6/24
 */

public class SnackAnimationUtil {
    private Component mTargetView;
    private long mAutoDismissTime;
    private AnimatorProperty mSnackInAnim;
    private AnimatorProperty mSnackOutAnim;
    private SnackAnimationCallback mSnackAnimationCallback;

    public static SnackAnimationUtil with(AnimatorProperty snackInRes, AnimatorProperty snackOutRes) {
        return new SnackAnimationUtil(snackInRes, snackOutRes);
    }

    private SnackAnimationUtil(AnimatorProperty snackInRes, AnimatorProperty snackOutRes) {
        mSnackInAnim = snackInRes;
        mSnackOutAnim = snackOutRes;
    }

    public SnackAnimationUtil setDismissDelayTime(long autoDismissTime) {
        mAutoDismissTime = autoDismissTime;
        return this;
    }

    public SnackAnimationUtil setTarget(Component playView) {
        mTargetView = playView;
        return this;
    }

    public SnackAnimationUtil setDismissDelayCallback(SnackAnimationCallback snackAnimationCallback) {
        mSnackAnimationCallback = snackAnimationCallback;
        return this;
    }

    public void play() {
        if (mTargetView == null || mSnackInAnim == null || mSnackOutAnim == null) {
            return;
        }
        mTargetView.setVisibility(Component.VISIBLE);
        mSnackInAnim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                playSnackOutAnimation();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        mSnackInAnim.setTarget(mTargetView);
        mSnackInAnim.start();
    }

    private void playSnackOutAnimation() {
        mSnackOutAnim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                mTargetView.setVisibility(Component.HIDE);
                if (mSnackAnimationCallback != null) {
                    mSnackAnimationCallback.dismissCallback();
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                mSnackOutAnim.setTarget(mTargetView);
                mSnackOutAnim.start();
            }
        }, mAutoDismissTime);
    }

    public interface SnackAnimationCallback {
        void dismissCallback();
    }
}
