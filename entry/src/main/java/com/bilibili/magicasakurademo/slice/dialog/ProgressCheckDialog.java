/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakurademo.slice.dialog;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import com.bilibili.magicasakurademo.ResourceTable;


/**
 * @author xyczero
 * @time 16/5/23
 */
public class ProgressCheckDialog extends CommonDialog {
    public static final String TAG = ProgressCheckDialog.class.getSimpleName();
    private final Component progressCheckDialog;

    public ProgressCheckDialog(Context context) {
        super(context);
        this.setTransparent(true);
        LayoutScatter inflater = LayoutScatter.getInstance(context);
        progressCheckDialog = inflater.getInstance(context).parse(ResourceTable.Layout_dialog_check_layout, null, false);
        setContentCustomComponent(progressCheckDialog);
        setSwipeToDismiss(true);
        progressCheckDialog.findComponentById(ResourceTable.Id_root).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                destroy();
            }
        });
        Button cancelButton = (Button) progressCheckDialog.findComponentById(ResourceTable.Id_button2);
        cancelButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                destroy();
            }
        });
        Button confirmButton = (Button) progressCheckDialog.findComponentById(ResourceTable.Id_button1);
        confirmButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                destroy();
            }
        });

    }

}
