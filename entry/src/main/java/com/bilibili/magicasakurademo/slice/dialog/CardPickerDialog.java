/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakurademo.slice.dialog;


import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

import com.bilibili.magicasakurademo.ResourceTable;
import com.bilibili.magicasakurademo.slice.utils.ThemeHelper;

/**
 * @author xyczero
 * @since 16/5/29
 */
public class CardPickerDialog extends CommonDialog implements Component.ClickedListener {
    public static final String TAG = "CardPickerDialog";
    private final Component cardPickerDialogView;
    Image[] mCards = new Image[8];
    Button mConfirm;
    Button mCancel;

    private int mCurrentTheme;
    private ClickListener mClickListener;

    public CardPickerDialog(Context context) {
        super(context);
        this.setTransparent(true);
        LayoutScatter inflater = LayoutScatter.getInstance(context);
        cardPickerDialogView = inflater.getInstance(context).parse(ResourceTable.Layout_dialog_theme_picker, null, false);

        setContentCustomComponent(cardPickerDialogView);
        setSwipeToDismiss(true);
        mCurrentTheme = ThemeHelper.getTheme(context.getApplicationContext());
        initview();
    }

    private void initview() {
        cardPickerDialogView.findComponentById(ResourceTable.Id_root).setClickedListener(this);
        mCancel = (Button) cardPickerDialogView.findComponentById(ResourceTable.Id_button2);
        mConfirm = (Button) cardPickerDialogView.findComponentById(ResourceTable.Id_button1);
        mCards[0] = (Image) cardPickerDialogView.findComponentById(ResourceTable.Id_theme_pink);
        mCards[1] = (Image) cardPickerDialogView.findComponentById(ResourceTable.Id_theme_purple);
        mCards[2] = (Image) cardPickerDialogView.findComponentById(ResourceTable.Id_theme_blue);
        mCards[3] = (Image) cardPickerDialogView.findComponentById(ResourceTable.Id_theme_green);
        mCards[4] = (Image) cardPickerDialogView.findComponentById(ResourceTable.Id_theme_green_light);
        mCards[5] = (Image) cardPickerDialogView.findComponentById(ResourceTable.Id_theme_yellow);
        mCards[6] = (Image) cardPickerDialogView.findComponentById(ResourceTable.Id_theme_orange);
        mCards[7] = (Image) cardPickerDialogView.findComponentById(ResourceTable.Id_theme_red);
        setImageButtons(mCurrentTheme);
        for (Image card : mCards) {
            card.setClickedListener(this);
        }
        mCancel.setClickedListener(this);
        mConfirm.setClickedListener(this);
    }

    private void setImageButtons(int currentTheme) {
        mCards[0].setSelected(currentTheme == ThemeHelper.CARD_SAKURA);
        mCards[1].setSelected(currentTheme == ThemeHelper.CARD_HOPE);
        mCards[2].setSelected(currentTheme == ThemeHelper.CARD_STORM);
        mCards[3].setSelected(currentTheme == ThemeHelper.CARD_WOOD);
        mCards[4].setSelected(currentTheme == ThemeHelper.CARD_LIGHT);
        mCards[5].setSelected(currentTheme == ThemeHelper.CARD_THUNDER);
        mCards[6].setSelected(currentTheme == ThemeHelper.CARD_SAND);
        mCards[7].setSelected(currentTheme == ThemeHelper.CARD_FIREY);
    }


    @Override
    protected void onWindowConfigUpdated(WindowManager.LayoutConfig configParam) {
        super.onWindowConfigUpdated(configParam);

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_root:
                destroy();
                break;
            case ResourceTable.Id_button1:
                if (mClickListener != null) {
                    mClickListener.onConfirm(mCurrentTheme);
                }
                destroy();
                break;
            case ResourceTable.Id_button2:
                destroy();
                break;
            case ResourceTable.Id_theme_pink:
                mCurrentTheme = ThemeHelper.CARD_SAKURA;
                setImageButtons(mCurrentTheme);
                break;
            case ResourceTable.Id_theme_purple:
                mCurrentTheme = ThemeHelper.CARD_HOPE;
                setImageButtons(mCurrentTheme);
                break;
            case ResourceTable.Id_theme_blue:
                mCurrentTheme = ThemeHelper.CARD_STORM;
                setImageButtons(mCurrentTheme);
                break;
            case ResourceTable.Id_theme_green:
                mCurrentTheme = ThemeHelper.CARD_WOOD;
                setImageButtons(mCurrentTheme);
                break;
            case ResourceTable.Id_theme_green_light:
                mCurrentTheme = ThemeHelper.CARD_LIGHT;
                setImageButtons(mCurrentTheme);
                break;
            case ResourceTable.Id_theme_yellow:
                mCurrentTheme = ThemeHelper.CARD_THUNDER;
                setImageButtons(mCurrentTheme);
                break;
            case ResourceTable.Id_theme_orange:
                mCurrentTheme = ThemeHelper.CARD_SAND;
                setImageButtons(mCurrentTheme);
                break;
            case ResourceTable.Id_theme_red:
                mCurrentTheme = ThemeHelper.CARD_FIREY;
                setImageButtons(mCurrentTheme);
                break;
            default:
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        mClickListener = clickListener;
    }

    public interface ClickListener {
        void onConfirm(int currentTheme);
    }
}
