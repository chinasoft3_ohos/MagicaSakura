/*
 * Copyright (C) 2016 Bilibili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.magicasakurademo.slice.dialog;

import ohos.agp.components.AbsButton;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.RadioButton;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import com.bilibili.magicasakurademo.ResourceTable;

/**
 * @since 2021-05-22
 */
public class ProgressStyleDialog extends CommonDialog implements Component.ClickedListener {
    public static final String TAG = ProgressStyleDialog.class.getSimpleName();
    private RadioButton mRadioButton1;
    private RadioButton mRadioButton2;

    public ProgressStyleDialog(Context context) {
        super(context);
        this.setTransparent(true);
        LayoutScatter inflater = LayoutScatter.getInstance(context);
        Component component = inflater.getInstance(context).parse(ResourceTable.Layout_dialog_progress_layout, null, false);
        setContentCustomComponent(component);
        setSwipeToDismiss(true);
        mRadioButton1 = (RadioButton) component.findComponentById(ResourceTable.Id_progress_style_1);
        mRadioButton2 = (RadioButton) component.findComponentById(ResourceTable.Id_progress_style_2);
        mRadioButton1.setClickedListener(this);
        mRadioButton2.setClickedListener(this);
        component.findComponentById(ResourceTable.Id_root).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                destroy();
            }
        });
        mRadioButton1.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                if (b) {
                    mRadioButton2.setChecked(false);
                }
            }
        });
        mRadioButton2.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                if (b) {
                    mRadioButton1.setChecked(false);
                }
            }
        });
    }

    @Override
    public void onClick(Component component) {
    }
}
